// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Gridsome',
  pathPrefix: 'dicefit', // https://gridsome.org/docs/deploy-to-gitlab/
  outputDir: 'public', // https://gridsome.org/docs/deploy-to-gitlab/
  plugins: [
    {
      use: 'gridsome-source-google-sheets',
      options: {
        sheetId: process.env.SHEET_ID,  // https://gridsome.org/docs/environment-variables/
        apiKey: process.env.API_KEY,
        // type: 'TYPE_NAME', //Optional - default is googleSheet. Used for graphql queries.
      }
    }
  ]
}
